function datasetInfo = DataSetConfig( DATASET )
%DATASETCONFIG Summary of this function goes here
%   Detailed explanation goes here

    if(strcmp(DATASET ,'DRIVE'))
        datasetInfo.inputPath = '';
        datasetInfo.gtPath = '';
        datasetInfo.maskPath = '';
        datasetInfo.outputPath = '';
        
    elseif(strcmp(DATASET, 'STARE'))
        datasetInfo.inputPath = '';
        datasetInfo.gtPath = '';
        datasetInfo.maskPath = '';
        datasetInfo.outputPath = '';
        
    elseif(strcmp(DATASET, 'HRF'))
        datasetInfo.inputPath = '';
        datasetInfo.gtPath = '';
        datasetInfo.maskPath = '';
        datasetInfo.outputPath = '';
        
    end
        
        datasetInfo.maskNameFunction = getMaskNameFunction(DATASET);
        datasetInfo.gtNameFunction = getGTNameFunction(DATASET);
end

function maskNameFunction = getMaskNameFunction(DATASET)
    
    function maskName = getMaskNameDRIVE(filename)
        maskName = strcat(strtok(filename,'_'),'_training_mask.GIF');
    end
    if(strcmp(DATASET, 'DRIVE')
        maskNameFunction = @getMaskNameDRIVE;
        return;
    end
    
    function maskName = getMaskNameHRF(filename)
        maskName = strcat(strtok(filename, '.'),'_mask.tif');
    end

    if(strcmp(DATASET, 'HRF'))
        maskNameFunction = @getMaskNameHRF;
        return;
    end
end

function gtNameFunction = getGTNameFunction(DATASET)
    function gtFileName = getGTFileNameSTARE(fileName)
        gtFileName = strcat(fileName,'.vk.ppm');
    end
    if(strcmp(DATASET,'STARE'))
        gtNameFunction = @getGTFileNameSTARE;
        return;
    end
    function gtFileName = getGTFileNameDRIVE(fileName)
        gtFileName = strcat(strtok(fileName,'_'),'_manual1.GIF');    
    end

    if (strcmp(dataset, 'DRIVE'))
        gtNameFunction = @getGTFileNameDRIVE;
        return;
    end
    function gtFileName = getGTFileNameHRF(fileName)
        gtFileName = strcat(strtok(fileName, '.' ), '.tif');
    end

    if (strcmp(dataset, 'HRF'))
        gtNameFunction = @getGTFileNameHRF;
        return;
    end
end