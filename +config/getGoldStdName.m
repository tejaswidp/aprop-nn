function [ goldStdFile ] = getGoldStdName( dataset , fileName )
%GETGOLDSTDPATH maps fundus image names to the corresponding segmented
%image name based on the dataset
    if(strcmp(dataset,'STARE'))
        goldStdFile = strcat(strtok(fileName,'.'),'.vk.ppm');
    elseif (strcmp(dataset, 'DRIVE'))
            goldStdFile = strcat(strtok(fileName,'_'),'_manual1.GIF');
    elseif (strcmp(dataset, 'HRF_DIABETIC_RETINOPATHY') || strcmp(dataset, 'HRF_HEALTHY') ...
                || strcmp(dataset, 'HRF_GLAUCOMA'))
            goldStdFile = strcat(strtok(fileName, '.' ), '.tif');
    elseif(strcmp(dataset, 'DRIVE_TEST'))
            goldStdFile = strcat(strtok(fileName,'_'),'_manual1.GIF');
    end
    

end

