function methods = implementedMethods(class)
% This function returns all function handles related to a particular
% problem.
% Inputs: 
% class - 'name' of the problem (Vessel Segmentation, Eye mask etc)

    if(strcmp(class, 'Vessel Segmentation'))
        methods = { @vesselSegmentation.getVasculatureMatchedFilterResponse ...
                    @vesselSegmentation.getVasculatureMorphology ...
                    @vesselSegmentation.getVasculatureScaleSpace };
    end
    
    
end

        