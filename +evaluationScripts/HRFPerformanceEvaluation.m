function HRFPerformanceEvaluation( )
%HRFPERFORMANCEEVALUATION Summary of this function goes here
%   Detailed explanation goes here
DATASET = 'HRF_DIABETIC_RETINOPATHY';
inputPath = 'D:\dataSet\HRF\fundus\diabetic_retinopathy';
goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\diabetic_retinopathy';
outputPath = 'D:\dataSet\HRF\output\diabetic_retinopathy';
reportFile ='D:\dataSet\STARE\output\diabetic_retinopathy';

data = { DATASET, inputPath, goldStdPath, outputPath, reportFile};
evaluationScripts.evaluator(data);

DATASET = 'HRF_HEALTHY';
inputPath = 'D:\dataSet\HRF\fundus\healthy';
goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\healthy';
outputPath = 'D:\dataSet\HRF\output\healthy';
reportFile ='D:\dataSet\STARE\output\healthy';

data = [ DATASET, inputPath, goldStdPath, outputPath, reportFile];
evaluationScripts.evaluator(data);

DATASET = 'HRF_GLAUCOMA';
inputPath = 'D:\dataSet\HRF\fundus\glaucoma';
goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\glaucoma';
outputPath = 'D:\dataSet\STARE\output\glaucoma';
reportFile ='D:\dataSet\STARE\output\glaucoma\';

data = [ DATASET, inputPath, goldStdPath, outputPath, reportFile];
evaluationScripts.evaluator(data);

end

