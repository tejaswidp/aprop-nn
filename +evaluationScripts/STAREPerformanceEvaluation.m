function STAREPerformanceEvaluation(  )
%STAREPERFORMANCEEVALUATION Summary of this function goes here
%   Detailed explanation goes here
DATASET = 'STARE';
inputPath = 'D:\dataSet\STARE\fundus';
goldStdPath = 'D:\dataSet\STARE\labelled_vk';
outputPath = 'D:\dataSet\STARE\output';
reportFile ='D:\dataSet\STARE\output';

data  = {DATASET, inputPath, goldStdPath, outputPath, reportFile};
evaluationScripts.evaluator(data);
end

