function drivePerformanceEvaluation
DATASET = 'DRIVE';
inputPath = 'D:\dataSet\DRIVE\training\images';
goldStdPath = 'D:\dataSet\DRIVE\training\1st_manual';
outputPath = 'D:\dataSet\PhaseIII_dataset\Segmented';
reportFile ='D:\dataSet\PhaseIII_dataset\STARE_Vessel_Segmentation\report';

data  = {DATASET, inputPath, goldStdPath, outputPath, reportFile};
evaluationScripts.evaluator(data);
endend
