function evaluator( data )
%EVALUATOR This function evaluates the performance of the different
%segmentation approaches on a dataset whose properties (file paths etc..)
%are passed through the data matrix. The list of properties is given below

DATASET = data{1};
inputPath = data{2};
goldStdPath = data{3};
outputPath = data{4};
reportFile = data{5};

files = dir(inputPath);
results = [];
values = [];


for i=3:size(files)
    imageFile = files(i);
    try
        img = imread(fullfile(inputPath, imageFile.name));
    catch
        error = 'Failed to read file'
    end
    [~ ,name,~] = fileparts(imageFile.name);
    goldStd = imread(fullfile(goldStdPath,config.getGoldStdName(DATASET, name)));
    goldStd = imresize(goldStd , [480 640], 'bicubic');
    methods = config.implementedMethods('Vessel Segmentation');
    
    numMethods = size(methods);
    for j = 1:numMethods(2)
        img = imresize(img,[480 640],'bicubic');
        segmented = methods{j}(img);
        %mask = outerMask.getOuterMask(img);
        mask = outerMask.getStoredOuterMask(DATASET, imageFile.name);
        mask = imresize(mask, [480 640], 'bicubic');
        segmented = segmented & mask;
        [sensitivity, specificity,~] = getMeasureValue(segmented, goldStd,mask,name,func2str(methods{j}), outputPath);
        
        results = [results; name];
        values = [values; j , sensitivity,specificity];
    end
    
end

csvwrite([reportFile,'.csv'] , results);
csvwrite([reportFile, '_values.csv'], values);

end

