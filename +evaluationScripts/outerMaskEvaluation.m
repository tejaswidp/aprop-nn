function [  ] = outerMaskEvaluation(  )
%OUTERMASKEVALUATION Summary of this function goes here
%   Detailed explanation goes here
    inputPath = 'D:\dataset\subjectSet\study2\healthy';
    outputPath = 'D:\dataset\output\';
    files = dir(inputPath);
    for i = 4:size(files,1)
        name = files(i).name;
        image = imread(fullfile(inputPath,name));
        
        mask = outerMask.getNoisyImageOuterMask(image);
        image = image(:,:,2);
        image(~mask) = 0;
        imwrite(image, fullfile(outputPath,name));
    end
    

end

