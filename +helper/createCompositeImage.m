function  createCompositeImage( TP,TN,FP,FN, name, functionName, outputPath)
%CREATECOMPOSITEIMAGE Creates overlay image to visualize error
%   The overlay image consists of 
%   RED -  True positive,Green - False Positive and Blue - False Negative?                                             
   
    % save the 'overlay' image to a directory
    overlayImage = zeros(size(TP));
    overlayImage(:,:,1) = TP * 255;
    overlayImage(:,:,2) = FP * 255;
    overlayImage(:,:,3) = FN * 255;
    filename =  strcat(name,'_', strrep(functionName,'.','_'),'.tif');
    filepath = char(strcat(outputPath,filename));
    imwrite(overlayImage,filepath);


end

