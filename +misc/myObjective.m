function jaccardSimilarity = myObjective( param)
%MYOBJECT Summary of this function goes here
%   Detailed explanation goes here
    x = param(1);
    sa = param(2);
    fundusImg = imread('D:\dataSet\DRIVE\test\images\02_test.tif');
    
    inputImg = vesselSegmentation.getVasculatureMatchedFilterResponse(fundusImg, x, sa);
    refImg = imread('D:\dataSet\DRIVE\test\1st_manual\02_manual1.gif');
    mask = imread('D:\dataSet\DRIVE\test\mask\02_test_mask.gif');
    
    TP = inputImg & refImg & mask;
    TN = ~inputImg & ~refImg & mask;
    FP = inputImg & ~refImg & mask;
    FN = ~inputImg & refImg & mask;
    
    tp = sum(TP(:));
    tn = sum(TN(:));
    fp = sum(FP(:));
    fn = sum(FN(:));

    jaccardSimilarity = -(tp / (tp+fn+fp));
    
end

