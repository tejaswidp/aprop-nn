function  mask  = getOuterMasks( img )
%GETOUTERMASKS Get the outer mask for a Fundus Image
%   This function computes the binary image and looks for edges
%   in a small neighbourhood around the outer boundary. 
%   input: Colour image of the fundus 
%   output: A binary mask which eliminates the illumination noise
%   adjustable parameters: erosion structural element, morphological opening
%   structural element
%   TODO: make the structural element size a function of the noise width
    r = img(:,:,1);
    s = stretchlim(r);
    d = decorrstretch(r,'Tol',s);
    [l,em] = graythresh(d);
    bw = imfill(im2bw(d,l*(1-em)),'holes');
    
    
    inner = bwmorph(bw,'shrink',10) ;
    d(inner) = 0;  %remove the inner part of the fundus
    e = edge(d);
    e = imerode(~inner,strel('line',5,5))&e;       %remove the edge created by the mask
    
    noiseOutline = imerode(bw,strel('line',2,2)) & ~e;  %erode to remove the original boundary
    mask = imopen(noiseOutline ,strel('disk',10,4));
    mask = imgaussian(mask,6);

end

