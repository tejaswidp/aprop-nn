function [ mask ] = getOuterMask( img )
%GETOUTERMASK Used to compute the mask for the STARE dataset
% The top 1% and bottom 1% 
%   Detailed explanation goes here  
    r = img(:,:,1);
    s = stretchlim(r);
    d = decorrstretch(r,'Tol',s);
    [l,em] = graythresh(d);
    mask = imfill(im2bw(d,l*(1-em)),'holes');
    mask = bwmorph(mask,'shrink',10);
end

