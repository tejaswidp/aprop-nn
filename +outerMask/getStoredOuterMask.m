function [ mask ] = getStoredOuterMask( DATASET , filename )
%GETSTOREDOUTERMASK Summary of this function goes here
%   Detailed explanation goes here
    if(strcmp(DATASET,'DRIVE'))
        MASK_DIR = 'D:\dataSet\DRIVE\training\mask\';
        name = strcat(strtok(filename,'_'),'_training_mask.GIF');
        
    elseif(strcmp(DATASET, 'HRF_HEALTHY'))
        MASK_DIR = 'D:\dataSet\HRF\mask\healthy\';
        name = strcat(strtok(filename, '.'),'_mask.tif');
        
    elseif(strcmp(DATASET, 'HRF_GLAUCOMA'))
        MASK_DIR = 'D:\dataSet\HRF\mask\glaucoma\';
        name = strcat(strtok(filename, '.'),'_mask.tif');
        
    elseif(strcmp(DATASET, 'HRF_DIABETIC_RETINOPATHY'))
        MASK_DIR = 'D:\dataSet\HRF\mask\diabetic_retinopathy\';
        name = strcat(strtok(filename, '.'),'_mask.tif');
    elseif(strcmp(DATASET, 'DRIVE_TEST'))
        MASK_DIR = 'D:\dataSet\DRIVE\test\mask\';
        name = strcat(strtok(filename,'_'),'_test_mask.GIF');
    end
    
    mask = imread(strcat(MASK_DIR,name));
    if(size(mask,3) == 3)
        mask = mask(:,:,1);
    end
    mask = imerode(mask,strel('disk',5));

end

