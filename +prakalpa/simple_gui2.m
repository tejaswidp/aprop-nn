function simple_gui2
% SIMPLE_GUI2 Select a data set from the pop-up menu, then
% click one of the plot-type push buttons. Clicking the button
% plots the selected data in the axes.

%  Create and then hide the UI as it is being constructed.
f = figure('Visible','on','Position',[360,500,450,285]);

% Construct the components.
hrun    = uicontrol('Style','pushbutton',...
             'String','run','Position',[315,220,70,25],...
             'Callback',@surfbutton_Callback);
htext  = uicontrol('Style','text','String','Select method',...
           'Position',[325,90,60,15]);
hpopup = uicontrol('Style','popupmenu',...
           'String',{'scale space','motion blur','matched filter response'},...
           'Position',[300,50,100,25],...
           'Callback',@popup_menu_Callback);
       
p = uipanel(f,'Title','My Panel',...
             'Position',[0.5, .1 .5 .8]);

ha = axes('Parent',p,'Position',[1,1,0.5,0.5]);

ha1 = axes('Parent',p, 'Position', [ 0,0,0.5,0.25,]);
imagePath = 'D:\dataSet\DRIVE\training\images\21_training.tif';
current_data = 'scale space';
image = imread(imagePath);
imshow(image);
align([hrun,htext,hpopup],'Center','None');
align([ha,ha1], 'left','top');

% Initialize the UI.
% Change units to normalized so components resize automatically.
f.Units = 'normalized';
ha.Units = 'normalized';
ha1.Units = 'normalized';
hrun.Units = 'normalized';

htext.Units = 'normalized';
hpopup.Units = 'normalized';



% Assign the a name to appear in the window title.
f.Name = 'Simple GUI';

% Move the window to the center of the screen.
%movegui(f,'center')

% Make the window visible.
f.Visible = 'on';

%  Pop-up menu callback. Read the pop-up menu Value property to
%  determine which item is currently displayed and make it the
%  current data. This callback automatically has access to 
%  current_data because this function is nested at a lower level.
   function popup_menu_Callback(source,eventdata) 
      % Determine the selected data set.
      str = get(source, 'String');
      val = get(source,'Value');
      % Set current data to the selected data set.
      current_data = str{val};
   end

  % Push button callbacks. Each callback plots current_data in the
  % specified plot type.

  function surfbutton_Callback(source,eventdata) 
      if(strcmp(current_data, 'motion blur'))
         segmented = vesselSegmentation.getVasculatureMotionBlur(image,9);
      elseif(strcmp(current_data, 'scale space')) % User selects Membrane.
         segmented = vesselSegmentation.getVasculatureScaleSpace(image,[3,11],2, 0.8,[3,7]);
      elseif(strcmp(current_data, 'matched filter response')) % User selects Sinc.
         segmented = vesselSegmentation.getVasculatureMatchedFilterResponse(image, 7, 0.1);
      end
      axes(ha1);
      segmented = vesselSegmentation.connectedComponentThres(segmented, 0);
      imshow(segmented);
  end

 
end