function [result, files]= motionBlur( LB,UB,STEP_SIZE,DATASET )
%MOTIONBLUR Summary of this function goes here
%   Detailed explanation goes here
    if(strcmp(DATASET,'DRIVE'))
        imagePath = 'D:\dataSet\DRIVE\training\images\';
        goldStdPath = 'D:\dataSet\DRIVE\training\1st_manual\';

    elseif(strcmp(DATASET,'STARE'))
        imagePath = 'D:\dataSet\STARE\fundus\';
        goldStdPath = 'D:\dataSet\STARE\labelled_vk\';
    elseif(strcmp(DATASET,'HRF_HEALTHY'))
        imagePath = 'D:\dataSet\HRF\fundus\healthy\';
        goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\healthy\';
    elseif(strcmp(DATASET, 'HRF_GLAUCOMA'))
        imagePath = 'D:\dataSet\HRF\fundus\glaucoma\';
        goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\glaucoma\';
    elseif(strcmp(DATASET, 'HRF_DIABETIC_RETINOPATHY'))
        imagePath = 'D:\dataSet\HRF\fundus\diabetic_retinopathy\';
        goldStdPath = 'D:\dataSet\HRF\vessel_gold_std\diabetic_retinopathy\';
    elseif(strcmp(DATASET, 'DRIVE_TEST'))
        imagePath = 'D:\dataSet\DRIVE\test\images\';
        goldStdPath = 'D:\dataSet\DRIVE\test\1st_manual\';
    end
%     fundusImages = read(fundusImagePath);
%     maskImages = read(maskPath);
%     gtImage = read(gtPath);
    files = dir(imagePath);
    result = zeros((UB-LB+1) * size(files,1)-3,5);
    idx = 1;
    for i = LB:STEP_SIZE:UB
        a = ['running for LEN = ', num2str(i)]
        for j = 3:size(files,1)
            filename = files(j).name;
            if(strcmp(filename,'Thumbs.db'))
                continue;
            end
            image = imread(strcat(imagePath ,filename));
            if(strcmp(DATASET,'STARE'))
                mask = outerMask.getOuterMask(image);
            else
                mask = outerMask.getStoredOuterMask(DATASET, filename);
            end
            groundtruth = imread(strcat(goldStdPath,config.getGoldStdName(DATASET, filename)));
            
                        
            if(strcmp(strtok(DATASET,'_'),'HRF'))
                image = imresize(image,[480,700]);
                mask = imresize(mask,[480,700]);
                groundtruth = imresize(groundtruth,[480,700]);
            end
            segmented = vesselSegmentation.getVasculatureMotionBlur(image, i);
            postProcessed = vesselSegmentation.connectedComponentThres(segmented,0);
            [sens,spec,tp,tn,fp,fn] = getMeasureValue(postProcessed,groundtruth,mask,'','','');
            
            similarity = tp / (tp+fp+fn);
            result(idx,:) = [i,j,sens,spec,similarity];
            idx = idx+1;
        end
    
    end
    
end


