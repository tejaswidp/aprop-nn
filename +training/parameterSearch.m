function  parameterSearch( )
%PARAMETERSEARCH Summary of this function goes here
%   Detailed explanation goes here
MIN_VESSEL_WIDTH = 5.0;
MAX_VESSEL_WIDTH = 15.0;

SA_STEP_SIZE = 0.1;
imagePath = 'D:\dataSet\DRIVE\training\images\';
goldStdPath = 'D:\dataSet\DRIVE\training\1st_manual\';
DATASET = 'DRIVE';
files = dir(imagePath);
for fi= 3:size(files,1)
    filename = files(fi).name;
    image = imread(strcat(imagePath ,filename));
    mask = outerMask.getStoredOuterMask(DATASET,filename);
    groundtruth = imread(strcat(goldStdPath,config.getGoldStdName(DATASET, filename)));
    result = zeros( (MAX_VESSEL_WIDTH - MIN_VESSEL_WIDTH)*(1/SA_STEP_SIZE),8);
    entry=1;
    resultFileName = {};
    for i=MIN_VESSEL_WIDTH:MAX_VESSEL_WIDTH
        similarity = 0;
        for j=0.1:SA_STEP_SIZE:1
            segmented = vesselSegmentation.getVasculatureMatchedFilterResponse(image,[-i:i],j);
            [sensitivity,specificity,tp,tn,fp,fn] = getMeasureValue( segmented,groundtruth,mask,'','','');
            
            
            newsimilarity = tp / (tp+fn+fp);
            if(newsimilarity < similarity || j==1)
               result(entry,:) = [i,j,sensitivity,specificity,tp,tn,fp,fn];
               entry=entry+1;
               resultFileName = {resultFileName,{filename}};
               break;
            end
            similarity = newsimilarity;
        end
    end
end

end

