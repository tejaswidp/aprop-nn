function [ centerlineNetwork ] = centerLineDetection( img )
%CENTERLINEDETECTION Summary of this function goes here
%   Detailed explanation goes here
    grayImg = img(:,:,2);
    kernel = [-1,-2,0,2,1;-2,-4,0,4,2;-1,-2,0,2,1];
    for i = 0:90:270
        r = imfilter(double(grayImg),imrotate(kernel,i));
        centerlineNetwork = getCenterLine(r);
    end
       % ordfilt2(r,1,ones(1,4))+ ordfilt2(r,4,ones(1,4));

end

function cl = getCenterLine(o)
cl = zeros(size(o));
    for i = 1:size(o,1)
        for j = 1:size(o,2)
                if(o(i,j) > 0 && o(i,j+1) > 0 &&... 
                    o(i,j+2) <0 && o(i,j+3) <0)
                    cl(i,j+1) = max(o(i,j),o(i,j+1)) + abs(min(o(i,j+2),o(i,j+3)));
                end
        end
    end
end

            
