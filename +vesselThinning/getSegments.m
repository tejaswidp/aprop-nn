function segmentedImg = getSegments(img)
% use this code to thin your vessel network. 
% input is the stack of vascular networks.
   
    vascular = imfill(img,'holes'); 
    vascular = bwmorph(vascular, 'majority'); 
    vascularThin = bwmorph(vascular,'thin',Inf);
    %% Clean up the terminal spurs in each vessel segment, get thin skeleton
    neighbour_count = imfilter(uint8(vascularThin), ones(3));
    bw_branches = neighbour_count > 3 & vascularThin;
    bw_ends = neighbour_count <= 2 & vascularThin;
    
    bw_segments = vascularThin & ~bw_branches;
    bw_terminal = imreconstruct(bw_ends, bw_segments);
    vascularThin(bw_terminal & ~bwareaopen(bw_terminal, 10)) = false;
    bw_thin = bwmorph(vascularThin, 'spur');
    bw_thin = bwmorph(bw_thin, 'thin', Inf);
    
    neighbour_count = imfilter(uint8(bw_thin), ones(3));
    bw_branches = neighbour_count > 3 & bw_thin;
    bw_segments = bw_thin & ~bw_branches;
    
    cc  = bwconncomp(bw_segments);
    lmat = labelmatrix(cc);
    stat = regionprops(cc,'Area','Orientation','PixelIdxList');
    area = [stat.Area];
    
    high = find(area < mean(area));
    segmentedImg = bw_segments - ismember(lmat,high);   
  
    
    
end