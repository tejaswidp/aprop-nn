
This repository contains Matlab code of algorithms which could be used
for blood vessel segmentation from fundus images. 

The algorithms are from [this
ieee](http://ieeexplore.ieee.org/document/7911975/) paper. It includes the algorithms,  data and all pieces of the processing pipeline.
