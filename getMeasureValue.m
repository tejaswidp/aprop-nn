function [sensitivity, specificity ,tp,tn,fp,fn] = getMeasureValue(inputImg, refImg , mask,name, functionName, outputPath)
% Computes binary classification values
% Inputs:
% inputImg - binary vessel segmented image
% refImg - binary vessel gold standard image
% Outputs:
% sensitivity - detected segment / all detected
% specificity - unmatched segment/
% tp,tn,fp,fn -  (t: true, f: false, p: positive, n: negative)
% TODO: Remove name, functionName from the argument list
    % 1 white 0 black
    TP = inputImg & refImg & mask;
    TN = ~inputImg & ~refImg & mask;
    FP = inputImg & ~refImg & mask;
    FN = ~inputImg & refImg & mask;
    
    %helper.createCompositeImage(TP,TN,FP,FN, name, functionName, outputPath);
    tp = sum(TP(:));
    tn = sum(TN(:));
    fp = sum(FP(:));
    fn = sum(FN(:));

    sensitivity = tp / (tp + fn);
    specificity = tn / (tn + fp);
    
end







